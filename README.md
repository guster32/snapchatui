![](snapchat.ico)
Ui Clone Sample ReacNative clone ui of SnapChat. I followed youtube tutorial from [here](https://www.youtube.com/watch?v=pgDOpIm6ozQ) and [here](https://www.youtube.com/watch?v=Y-0Iy0EntWY) with the exception that I did not use Expo and I used typescript. This code does not work on android. Apperently according to [this](https://medium.com/the-react-native-log/implement-snapchat-like-swipe-navigation-declaratively-in-react-native-309e71229c89) android vertical scroll is not supported by the library used for swiping screens [react-native-swiper]().
>"Unfortunately, vertical navigation is not supported on Android yet. I might be taking a crack at it soonish."

Also according to the author of [this](https://github.com/archriss/react-native-snap-carousel) other library android has performance problems when debugging. 

> "Android's debug mode is a mess: timeouts regularly desynchronize and scroll events are fired with some lag, which completely alters the inner logic of the carousel. On Android, you *will* experience issues with carousel's behavior when JS Dev Mode is enabled, and you *might* have trouble with unreliable callbacks and loop mode when it isn't**. This is unfortunate, but it's rooted in various flaws of `ScrollView`/`FlatList`'s implementation and the miscellaneous workarounds we had to implement to compensate for it."

![](snapclone.gif)
import { AppRegistry } from 'react-native';
import App from './build/main';

AppRegistry.registerComponent('bustamove', () => App);
